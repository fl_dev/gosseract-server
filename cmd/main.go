package main

import "gosseract-server/internal/api/server"

func main() {
	e := server.NewServer()
	e.Logger.Fatal(e.Start(":7070"))
}