# Gosseract Server

This is an API wrapper around the gosseract library for optical character recognition. 

## Running the service

- `docker build -t gosseract-server .`
- `docker run -p 7070:7070 gosseract-server`
