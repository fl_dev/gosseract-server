package server

import (
	"gosseract-server/internal/api/handlers"
	"gosseract-server/internal/logic"

	"github.com/labstack/echo/v4"
)

func NewServer() *echo.Echo {
	e := echo.New()

	l := logic.NewLogic()
	h := handlers.NewHandlers(l)
	e.POST("/api/image-text", h.ImageText)
	e.POST("/api/image-bounding-boxes", h.GetBoundingBoxes)
	return e
}