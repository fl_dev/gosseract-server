package handlers

import "gosseract-server/internal/logic"

type Handlers struct{
	Logic *logic.Logic
}

func NewHandlers(logic *logic.Logic) *Handlers {
	return &Handlers{
		Logic: logic,
	}
}