package handlers

import (
	"fmt"
	"gosseract-server/internal/logic"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handlers) GetBoundingBoxes(c echo.Context) error {
	input := &logic.Image{}

	fileHeader, err := c.FormFile("image")
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	levelStr := c.FormValue("page_iterator_level")
	level, err := strconv.Atoi(levelStr)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("Could not convert page_iterator_level to int: %w", err))
	}
	
	input.FileHeader = fileHeader

	boudningBoxes, err := h.Logic.GetBoundingBoxes(input, level)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, boudningBoxes)
}