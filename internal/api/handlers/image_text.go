package handlers

import (
	"gosseract-server/internal/logic"
	"net/http"

	"github.com/labstack/echo/v4"
)

func (h *Handlers) ImageText(c echo.Context) error {
	input := &logic.Image{}

	fileHeader, err := c.FormFile("image")
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	input.FileHeader = fileHeader

	imgText, err := h.Logic.ImageText(input)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, imgText)
}