package logic

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"

	"github.com/otiai10/gosseract"
)



type Logic struct {
	ocr *gosseract.Client
}

func NewLogic() *Logic {
	client := gosseract.NewClient()
	return &Logic{
		ocr: client,
	}
}

type Image struct {
	FileHeader *multipart.FileHeader `json:"image"`
}

type Text struct {
	ImageText string `json:"image_text"`
}

func (l *Logic) GetBoundingBoxes(input *Image, level int) ([]gosseract.BoundingBox, error) {
	boundingBoxes := []gosseract.BoundingBox{}
	bytes, err := l.fileToBytes(input)
	if err != nil {
		return boundingBoxes, fmt.Errorf("Failed to convert file to bytes: %w", err)
	}

	if err := l.ocr.SetImageFromBytes(bytes); err != nil {
		return boundingBoxes, fmt.Errorf("Failed to set OCR client image from input bytes: %w", err)
	}

	boundingBoxes, err = l.ocr.GetBoundingBoxes(gosseract.PageIteratorLevel(level))
	if err != nil {
		return boundingBoxes, fmt.Errorf("Failed to find boudning boxes: %w", err)
	}

	return boundingBoxes, nil
}

func (l *Logic) ImageText(input *Image) (*Text, error) {
	output := &Text{}
	
	bytes, err := l.fileToBytes(input)
	if err != nil {
		return output, fmt.Errorf("Failed to convert file to bytes: %w", err)
	}

	if err := l.ocr.SetImageFromBytes(bytes); err != nil {
		return output, fmt.Errorf("Failed to set OCR client image from input bytes: %w", err)
	}

	text, err := l.ocr.Text()
	if err != nil {
		return output, fmt.Errorf("Failed to read text from image: %w", err)
	}

	output.ImageText = text
	return output, nil
}

func (l *Logic) fileToBytes(input *Image) ([]byte, error) {
	file, err := input.FileHeader.Open()
	if err != nil {
		return nil, fmt.Errorf("input.FileHeader.Open(): %w", err)
	}
	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
    return nil, fmt.Errorf("io.Copy: %w", err)
	}

	return buf.Bytes(), nil
}